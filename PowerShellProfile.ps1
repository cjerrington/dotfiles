<#$Host.UI.RawUI.BackgroundColor = ($bckgrnd = 'Black')
$Host.UI.RawUI.ForegroundColor = 'DarkGray'
$Host.PrivateData.ErrorForegroundColor = 'Magenta'
$Host.PrivateData.ErrorBackgroundColor = $bckgrnd
$Host.PrivateData.WarningForegroundColor = 'Gray'
$Host.PrivateData.WarningBackgroundColor = $bckgrnd
$Host.PrivateData.DebugForegroundColor = 'Yellow'
$Host.PrivateData.DebugBackgroundColor = $bckgrnd
$Host.PrivateData.VerboseForegroundColor = 'DarkCyan'
$Host.PrivateData.VerboseBackgroundColor = $bckgrnd
$Host.PrivateData.ProgressForegroundColor = 'Gray'
$Host.PrivateData.ProgressBackgroundColor = $bckgrnd
#>


Set-Location C:\

Clear-Host

Set-Alias -Name grep -Value Select-String

<#
gci -r|sort -descending -property length | select -first 10 name, @{Name="Gigabytes";Expression={[Math]::round($_.length / 1GB, 2)}}
wmic /node:<computer name> process call create "cmd.exe /c netsh firewall set service RemoteDesktop enable"
wmic /node:<computer name> process call create 'cmd.exe /c reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f'
https://4sysops.com/archives/enable-remote-desktop-remotely-on-windows-10/
https://www.tenforums.com/tutorials/90033-enable-disable-ipv6-windows.html
#>

function pro { code $profile }
function reload { .$PROFILE; clear-host; Write-Host "Profile has been reloaded."}

function Show-Notification {
    [cmdletbinding()]
    Param (
        [string]
        $ToastTitle,
        [string]
        [parameter(ValueFromPipeline)]
        $ToastText
    )

    [Windows.UI.Notifications.ToastNotificationManager, Windows.UI.Notifications, ContentType = WindowsRuntime] > $null
    $Template = [Windows.UI.Notifications.ToastNotificationManager]::GetTemplateContent([Windows.UI.Notifications.ToastTemplateType]::ToastText02)

    $RawXml = [xml] $Template.GetXml()
    ($RawXml.toast.visual.binding.text|where {$_.id -eq "1"}).AppendChild($RawXml.CreateTextNode($ToastTitle)) > $null
    ($RawXml.toast.visual.binding.text|where {$_.id -eq "2"}).AppendChild($RawXml.CreateTextNode($ToastText)) > $null

    $SerializedXml = New-Object Windows.Data.Xml.Dom.XmlDocument
    $SerializedXml.LoadXml($RawXml.OuterXml)

    $Toast = [Windows.UI.Notifications.ToastNotification]::new($SerializedXml)
    $Toast.Tag = "PowerShell"
    $Toast.Group = "PowerShell"
    $Toast.ExpirationTime = [DateTimeOffset]::Now.AddMinutes(1)

    $Notifier = [Windows.UI.Notifications.ToastNotificationManager]::CreateToastNotifier("PowerShell")
    $Notifier.Show($Toast);
}

function Find-Item {

  param ( 

    [Parameter(Mandatory = $True)]
    [string]$item
    )

  (Get-childitem | ?{$_.name -like "*$item*"}).fullname
}

function newpass {
  python C:\scripts\python\randpass.py -n -a -l 14 -c 5
}
function Expand-URL{

  param ( 

  [Parameter(Mandatory = $True)]
  [uri]$URL
  )

  if(isURIWeb($URL)){
      $req = [System.Net.HttpWebRequest]::Create($URL)
      # Set user agent so request acts like it would in a browser
      $req.UserAgent=".NET Framework Test Client";
      $req.Method = "HEAD"
      $response = $req.GetResponse()
      $fUri = $response.ResponseUri
      $filename = [System.IO.Path]::GetFileName($fUri.LocalPath);
      $fUri.AbsoluteUri
      $response.Close()
  
      return $filename
  }else{
      write-host 'URL needs to be in a valid format: http or https'
      write-host "Received: $URL"
  }
}

function isURIWeb($address) {
  $uri = $address -as [System.URI]
  $uri.AbsoluteURI -ne $null -and $uri.Scheme -match '[http|https]'
}

function pypipupgrade { python -m pip install --upgrade pip setuptools wheel }

function pip-upgradeall {
  # https://www.activestate.com/resources/quick-reads/how-to-update-all-python-packages/
  pip freeze | %{$_.split('==')[0]} | %{pip install --upgrade $_} 
}

function pybuild {
  python setup.py sdist bdist_wheel
}

function pyupload {
  twine upload dist/*
}
function cdpy { cd "C:\scripts\python" }

function git-update { git pull origin master }

function git-push {
  git remote -v
	$m = Read-Host "What did you add or change?"
	git add .
	git commit -m $m
	git push origin master
}

function git-newpost {
	$title = Read-Host "What is the title of the post?"
	# update title to replace spaces with dashes so Jekyll will render 
	$t = $title.Replace(" ","-")
	$date = get-date -format ("yyyy-MM-dd")
	$file = "$($date)-$($t)"
	# uncomment next line if we want all words to be capitals
	#$file = (Get-Culture).TextInfo.ToTitleCase($file)
	Copy-Item "C:\Users\clayton.errington\GitHub\cjerrington.github.io\_drafts\2019-08-22-template.md" -Destination "C:\Users\clayton.errington\GitHub\cjerrington.github.io\_posts\$file.md"
	notepad++ "C:\Users\clayton.errington\GitHub\cjerrington.github.io\_posts\$file.md"
}

function repos {
  cd "C:\Users\clayton.errington\source\repos"
  Clear
  ls
}

function size {
  Param(
    [parameter(Mandatory=$false)]
    [String]
    $Folder = $ENV:USERPROFILE
    )

    Write-Host "Size of Folder $Folder is: "

    "{0:N2} GB" -f ((gci $Folder -r | measure Length -s).Sum /1GB)
}

function Get-SystemUptime{
    #$operatingSystem = Get-CimInstance -ClassName Win32_OperatingSystem | select LastBootUpTime
    $operatingSystem = Get-WmiObject Win32_OperatingSystem
    [Management.ManagementDateTimeConverter]::ToDateTime($operatingSystem.LastBootUpTime)
}

function Get-WifiPasswords{
  Write-Host "Current Wifi Connection:"
  netsh wlan show interfaces | Select-string '\sSSID'
  (netsh wlan show profiles) | Select-String "\:(.+)$" | %{$name=$_.Matches | % {$_.Groups[1].Value.Trim()}; $_} |%{(netsh wlan show profile name="$name" key=clear)} | Select-String "Key Content\W+\:(.+)$" | %{$pass=$_.Matches | % {$_.Groups[1].Value.Trim()}; $_} | %{[PSCustomObject]@{ PROFILE_NAME=$name;PASSWORD=$pass }} | Format-Table -AutoSize
}

function version{
  Write-host "PowerShell Version: $($psversiontable.psversion) - ExecutionPolicy: $(Get-ExecutionPolicy)"
  (Get-WmiObject Win32_OperatingSystem).Caption 
  [Environment]::OSVersion.VersionString
}

function cdr{
  Clear-Host
  Set-Location "C:\Users\clayton.errington\source\repos"
  Get-ChildItem
  Clear-Host
}
function Sort-Files {
  [CmdletBinding()]
  param (
      [Parameter(Mandatory=$false)]
      [string]$cwd = (Get-Location).Path
  )

  #$filecount = Get-Childitem $cwd -file | Group-Object Extension -NoElement | Sort-Object count -desc

  # Gather the extensions of the files in the folder path
  $extforfolders = Get-ChildItem $cwd -File | Select-Object Extension
  # Gather the list of files only
  $files = Get-ChildItem $cwd -File

  # Create a new list to add the extensions to
  $extensions = New-Object Collections.Generic.List[String]

  Write-Host "Getting files and creating subfolders of extentions if it does not exist..."
  foreach($folder in $extforfolders){
      # Use the .NET class to create a directory. If it exits will proceed. If not exists, will create the directory
      [System.IO.Directory]::CreateDirectory("$cwd\$($folder.Extension)") | Out-Null
      # Add every extension to the list. Will sort later.
      $extensions.Add($folder.Extension)
  }

  if($extensions.Count -gt 0){
      foreach($file in $files){
          try {
              Write-Host "Moving $($file) to folder: $cwd\$($file.Extension)"
              # If File exists or in use ErrorAction Stop so we can catch the error properly
              # Using $file.FullName to get long path of file since script/function may not always be in the same directory as files. 
              Move-Item $file.FullName -Destination "$cwd\$($file.Extension)" -Force -ErrorAction Stop
          }
          catch { 
              Write-Warning "Failed to move file '$file' to folder '$($file.Extension)'. File either exists in folder or is in use."
          }
      }
      Write-Host "Summary of Sorting Files"
      # Group and count the extentions
      $extensions | Group-Object -NoElement | Sort-Object count -Descending
  }else {
      Write-Host "No files to sort here: $cwd"
  }
  
}
function orgdownload{
  Sort-Files -cwd "C:\Users\clayton.errington\Downloads"
}

function orgdesk{
  Sort-Files -cwd "C:\Users\clayton.errington\Desktop"
}

function organize {
  orgdownload
  orgdesk
}

# Add nix-like touch command
function touch
{
    $file = $args[0]

    if($file -eq $null) 
    {
        Write-Host "No filename supplied"
        Write-Host ""
        Write-Host "Usage: touch file.txt"
        Write-Host "Will create file if it does not exist, and update the last modified time if it does."
        Write-Host "Similar to the unix command"
        break
    }

    if(Test-Path $file)
    {
        (Get-ChildItem $file).LastWriteTime = Get-Date
    }
    else
    {
        # UTF-16 LE
        # echo $null > $file

        # UTF-8 With BOM
        # $null | Out-File $file -Encoding "UTF8"
        
        # UTF-8 W/O BOM
        [IO.File]::WriteAllLines("$pwd\$file", "")
    }
}

Function MakeUp-String([Int]$Size = 16, [Char[]]$CharSets = "ULNS", [Char[]]$Exclude) {
	# https://powersnippets.com/create-password/
    $Chars = @(); $TokenSet = @()
    If (!$TokenSets) {$Global:TokenSets = @{
        U = [Char[]]'ABCDEFGHIJKLMNOPQRSTUVWXYZ'                                #Upper case
        L = [Char[]]'abcdefghijklmnopqrstuvwxyz'                                #Lower case
        N = [Char[]]'0123456789'                                                #Numerals
        S = [Char[]]'!@#$%^*_=+;'                                               #Symbols
    }}
    $CharSets | ForEach {
        $Tokens = $TokenSets."$_" | ForEach {If ($Exclude -cNotContains $_) {$_}}
        If ($Tokens) {
            $TokensSet += $Tokens
            If ($_ -cle [Char]"Z") {$Chars += $Tokens | Get-Random}             #Character sets defined in upper case are mandatory
        }
    }
    While ($Chars.Count -lt $Size) {$Chars += $TokensSet | Get-Random}
    $string = ($Chars | Sort-Object {Get-Random}) -Join ""                                #Mix the (mandatory) characters and output string
	write-host $string
    Set-Clipboard -Value $string
}; Set-Alias Create-Password MakeUp-String -Description "Generate a random string (password)"

# Uninstall a Windows program
function uninstall($programName)
{
    $app = Get-WmiObject -Class Win32_Product -Filter ("Name = '" + $programName + "'")
    if($app -ne $null)
    {
        $app.Uninstall()
    }
    else {
        Write-Output ("Could not find program '" + $programName + "'")
    }
}

Function Get-TotalWeekDays {

    <#
    .Synopsis
    Get total number of week days
    .Description
    Return the number of days between two dates not counting Saturday
    and Sunday.
    .Parameter Start
    The starting date
    .Parameter End
    The ending date
    .Example
    PS C:\> Get-TotalWeekDays -start 7/1/2012 -end 7/31/2012
    22
    .Inputs
    None
    .Outputs
    Integer
    #>

    [cmdletbinding()]

    Param (
    [Parameter(Position=0,Mandatory=$True,HelpMessage="What is the start date?")]
    [ValidateNotNullorEmpty()]
    [DateTime]$Start,
    [Parameter(Position=1,Mandatory=$True,HelpMessage="What is the end date?")]
    [ValidateNotNullorEmpty()]
    [DateTime]$End
    )

    Write-Verbose -message "Starting $($myinvocation.mycommand)"
    Write-Verbose -Message "Calculating number of week days between $start and $end"

    #define a counter
    $i=0
    #test every date between start and end to see if it is a weekend
    for ($d=$Start;$d -le $end;$d=$d.AddDays(1)){
      if ($d.DayOfWeek -notmatch "Sunday|Saturday") {
        #if the day of the week is not a Saturday or Sunday
        #increment the counter
        $i++    
      }
      else {
        #verify these are weekend days
        Write-Verbose ("{0} is {1}" -f $d,$d.DayOfWeek)
      }
    } #for

    #write the result to the pipeline
    $i

    Write-Verbose "Ending $($myinvocation.mycommand)"

} #end function

function Uninstall-App {
    Write-Output "Finding $($args[0]) ..."
    $uninstall32 = gci "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall" | foreach { gp $_.PSPath } | ? { $_ -match "$($args[0])" } | select UninstallString
    $uninstall64 = gci "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" | foreach { gp $_.PSPath } | ? { $_ -match "$($args[0])" } | select UninstallString

    if ($uninstall64) {
      $uninstall64 = $uninstall64.UninstallString -Replace "msiexec.exe","" -Replace "/I","" -Replace "/X",""
      $uninstall64 = $uninstall64.Trim()
      Write "Uninstalling..."
      start-process "msiexec.exe" -arg "/X $uninstall64 /qb" -Wait
    }
    if ($uninstall32) {
      $uninstall32 = $uninstall32.UninstallString -Replace "msiexec.exe","" -Replace "/I","" -Replace "/X",""
      $uninstall32 = $uninstall32.Trim()
      Write "Uninstalling..."
      start-process "msiexec.exe" -arg "/X $uninstall32 /qb" -Wait
    }
}

# Chocolatey profile
#$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
#if (Test-Path($ChocolateyProfile)) {
#  Import-Module "$ChocolateyProfile"
#}

function Rename-ComputerPS([string]$newComputerName)
{
	$computerInfo = gwmi Win32_ComputerSystem
	$computerInfo.rename($newComputerName)
}

Function backup{
	$backupDrive = (Get-Volume -FileSystemLabel "TylerTech Backup").Driveletter
	$backupDrive = $backupDrive + ":\"

	cd $backupDrive

	.\Win10Backup.ps1
}

Function comp{

  Param(
    [parameter(Mandatory=$true)]
    [String]
    $computer = $env:computername
    )

    #Begin string compare to get TTU
    #$referenz = @('TTU')
    #$referenzRegex = [string]::Join('|', [Regex]::Escape($referenz)) # create the regex

    if($computer -match $referenzRegex){
        #Write-Host "String contains TTU"
    }else{
        #Write-Host "string does not contain TTU, appending TTU Prefix"
       # $computer = "TTU$computer"
    }

    #Uppercase string for TTU
    $computer = $computer.ToUpper()

	   Try{
		     Get-ADComputer -Identity $computer -Properties name, distinguishedname, description, OperatingSystem, IPv4Address | Select-Object Name, DistinguishedName, Description, OperatingSystem, IPv4Address | format-List
	   }catch{
		     Write-host "`t$computer" -foregroundcolor "Red" -NoNewLine; Write-host " could not be found"
	      }
}

Function user{

  Param(
    [parameter(Mandatory=$true)]
    [String]
    $user = $env:username
    )
    Try{
     Get-ADPrincipalGroupMembership $user | select name
    }catch{
     Write-host "     $user" -foregroundcolor "Red" -NoNewLine; Write-host " could not be found"
    }
}

Function userinfo{

  Param(
    [parameter(Mandatory=$true)]
    [String]
    $user = $env:username
    )
    Try{
     Get-ADUser -Identity $User | select Samaccountname,GivenName,Surname,EmailAddress
     (Get-ADUser $User -properties mail).mail
    }catch{
     Write-host "     $user" -foregroundcolor "Red" -NoNewLine; Write-host " could not be found"
    }
}


function groupmembers{

Param(
    [parameter(Mandatory=$true)]
    [String]
    $group = "helpdesk_desktop_staff"
    )
    Try{
	Write-Host "Full details below:"
    	Get-ADGroupMember -Identity $group | foreach-object {
    		#Write-Host $_.SamAccountName
		userinfo $_.SamAccountName
 	}
    }catch{
     Write-host "`t$group" -foregroundcolor "Red" -NoNewLine; Write-host " could not be found"
    }


}

Function info{
#  Write-host "     $user" -foregroundcolor "Red" -NoNewLine; Write-host " could not be found"
	Write-Host "For computer: " -NoNewLine; Write-Host $env:computername -foregroundcolor "Red"

	$uptime = Get-Systemuptime
  Write-Host "The computer has been online since: " -NoNewLine; Write-Host $uptime -foregroundcolor "Red"

  $serial = Get-WMIObject -Class Win32_Bios | Select -expand serialnumber
  Write-Host "The serial number is: " -NoNewLine; Write-Host $serial -foregroundcolor "Red"

  $ip=[System.Net.Dns]::GetHostAddresses($computername) | where {$_.AddressFamily -notlike "InterNetworkV6"} | foreach {
    Write-Host "The IP Address is: " -NoNewLine; Write-Host $_.IPAddressToString -foregroundcolor "Red"
   }

  Write-Host "The Connected MAC address is: " -NoNewLine; Write-Host (Get-WmiObject win32_networkadapterconfiguration -ComputerName $env:COMPUTERNAME | Where{$_.IpEnabled -Match "True"} | Select-Object -Expand macaddress) -foregroundcolor "Red"

  $OS = (Get-WmiObject -class Win32_OperatingSystem).Caption

  If ([Environment]::Is64BitProcess-eq $True){
    Write-Host "OS is: " -NoNewLine; Write-Host "$OS 64 bit" -foregroundcolor "Red"
  }else{
    Write-Host "OS is: " -NoNewLine; Write-Host "$OS 32 bit" -foregroundcolor "Red"
  }

  $Ram = [Math]::Round((Get-WmiObject -Class Win32_ComputerSystem).TotalPhysicalMemory/1GB)
  Write-Host "RAM: " -NoNewLine; Write-Host "$Ram GB" -foregroundcolor "Red"

  $elevated = ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)

  Write-Host "Running as Admin: " -NoNewLine; Write-host $elevated -foregroundcolor "Red"

  $UAC = (Get-ItemProperty HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System).EnableLUA
  If($UAC -eq $true){
    Write-Warning "UAC Is enabled"
    $UACPrompt = Read-Host 'Would you like to disable UAC Y or N'
    if ($UACPrompt -eq 'Y'){
      Set-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System" -Name "EnableLUA" -Value "0"
      Write-Warning "Please Restart computer before continuing..."
    }
  }else{
    Write-Host "UAC is: "-NoNewLine; Write-Host "disabled" -foregroundcolor "Red"
  }

  $user = $env:USERNAME;
  $group = "Administrators";
  $groupObj =[ADSI]"WinNT://./$group,group"
  $membersObj = @($groupObj.psbase.Invoke("Members"))
  $members = ($membersObj | foreach {$_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)})

  If ($members -contains $user) {
   Write-Host $user -foregroundcolor "Red" -NoNewLine; Write-Host " exists in the local group $group"
  } Else {
   Write-Host "$user not exists in the local group $group"
  }

  $NetVer = (Get-ItemProperty -Path 'HKLM:\Software\Microsoft\NET Framework Setup\NDP\v4\Full' -ErrorAction SilentlyContinue).Version
  Write-Host "You have .NET Version " -NoNewLine; Write-Host $NetVer -foregroundcolor "Red"

  $ExtIP = curl http://ipecho.net/plain | Select -expand Content
  Write-Host "External IP: " -NoNewLine; Write-Host $ExtIP -foregroundcolor "Red"
  #curl http://ipecho.net/plain | Select-Object  @{label="External IP";expression={$_.Content}} | Format-List
}

Function Test-ConsoleColor {
  [cmdletbinding()]
  Param()

  Clear-Host
  $heading = "White"
  Write-Host "Pipeline Output" -ForegroundColor $heading
  Get-Service | Select -first 5

  Write-Host "`nError" -ForegroundColor $heading
  Write-Error "I made a mistake"

  Write-Host "`nWarning" -ForegroundColor $heading
  Write-Warning "Let this be a warning to you."

  Write-Host "`nVerbose" -ForegroundColor $heading
  $VerbosePreference = "Continue"
  Write-Verbose "I have a lot to say."
  $VerbosePreference = "SilentlyContinue"

  Write-Host "`nDebug" -ForegroundColor $heading
  $DebugPreference = "Continue"
  Write-Debug "`nSomething is bugging me. Figure it out."
  $DebugPreference = "SilentlyContinue"

  Write-Host "`nProgress" -ForegroundColor $heading
  1..10 | foreach -Begin {$i=0} -process {
   $i++
   $p = ($i/10)*100
   Write-Progress -Activity "Progress Test" -Status "Working" -CurrentOperation $_ -PercentComplete $p
   Start-Sleep -Milliseconds 250
  }
} #Test-ConsoleColor

Function Export-ConsoleColor {

  [cmdletbinding(SupportsShouldProcess)]
  Param(
  [Parameter(Position=0)]
  [ValidateNotNullorEmpty()]
  [string]$Path = '.\PSConsoleSettings.csv'
  )

  #verify this is the console and not the ISE
  if ($host.name -eq 'ConsoleHost') {
   $host.PrivateData | Add-Member -MemberType NoteProperty -Name ForegroundColor -Value $host.ui.rawui.ForegroundColor -Force
   $host.PrivateData | Add-Member -MemberType NoteProperty -Name BackgroundColor -Value $host.ui.rawui.BackgroundColor -Force
   Write-Verbose "Exporting to $path"
   Write-verbose ($host.PrivateData | out-string)
   $host.PrivateData | Export-CSV -Path $Path -Encoding ASCII -NoTypeInformation
  }
  else {
      Write-Warning "This only works in the console host, not the ISE."

  }
} #Export-ConsoleColor -Path c:\work\pretty.csv

Function Import-ConsoleColor {
  [cmdletbinding(SupportsShouldProcess)]
  Param(
  [Parameter(Position=0)]
  [ValidateScript({Test-Path $_})]
  [string]$Path = '.\PSConsoleSettings.csv'
  )

  #verify this is the console and not the ISE
  if ($host.name -eq 'ConsoleHost') {
      Write-Verbose "Importing color settings from $path"
      $data = Import-CSV -Path $Path
      Write-Verbose ($data | out-string)

      if ($PSCmdlet.ShouldProcess($Path)) {
          $host.ui.RawUI.ForegroundColor = $data.ForegroundColor
          $host.ui.RawUI.BackgroundColor = $data.BackgroundColor
          $host.PrivateData.ErrorForegroundColor = $data.ErrorForegroundColor
          $host.PrivateData.ErrorBackgroundColor = $data.ErrorBackgroundColor
          $host.PrivateData.WarningForegroundColor = $data.WarningForegroundColor
          $host.PrivateData.WarningBackgroundColor = $data.WarningBackgroundColor
          $host.PrivateData.DebugForegroundColor = $data.DebugForegroundColor
          $host.PrivateData.DebugBackgroundColor = $data.DebugBackgroundColor
          $host.PrivateData.VerboseForegroundColor = $data.VerboseForegroundColor
          $host.PrivateData.VerboseBackgroundColor = $data.VerboseBackgroundColor
          $host.PrivateData.ProgressForegroundColor = $data.ProgressForegroundColor
          $host.PrivateData.ProgressBackgroundColor = $data.ProgressBackgroundColor

          Clear-Host
      } #should process

  }
  else {
     Write-Warning "This only works in the console host, not the ISE."
  }

} #Import-ConsoleColor

Add-Type -As System.IO.Compression.FileSystem

function New-ZipFile {
	#.Synopsis
	#  Create a new zip file, optionally appending to an existing zip...
	[CmdletBinding()]
	param(
		# The path of the zip to create
		[Parameter(Position=0, Mandatory=$true)]
		$ZipFilePath,

		# Items that we want to add to the ZipFile
		[Parameter(Position=1, Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
		[Alias("PSPath","Item")]
		[string[]]$InputObject = $Pwd,

		# Append to an existing zip file, instead of overwriting it
		[Switch]$Append,

		# The compression level (defaults to Optimal):
		#   Optimal - The compression operation should be optimally compressed, even if the operation takes a longer time to complete.
		#   Fastest - The compression operation should complete as quickly as possible, even if the resulting file is not optimally compressed.
		#   NoCompression - No compression should be performed on the file.
		[System.IO.Compression.CompressionLevel]$Compression = "Optimal"
	)
	begin {
		# Make sure the folder already exists
		[string]$File = Split-Path $ZipFilePath -Leaf
		[string]$Folder = $(if($Folder = Split-Path $ZipFilePath) { Resolve-Path $Folder } else { $Pwd })
		$ZipFilePath = Join-Path $Folder $File
		# If they don't want to append, make sure the zip file doesn't already exist.
		if(!$Append) {
			if(Test-Path $ZipFilePath) { Remove-Item $ZipFilePath }
		}
		$Archive = [System.IO.Compression.ZipFile]::Open( $ZipFilePath, "Update" )
	}
	process {
		foreach($path in $InputObject) {
			foreach($item in Resolve-Path $path) {
				# Push-Location so we can use Resolve-Path -Relative
				Push-Location (Split-Path $item)
				# This will get the file, or all the files in the folder (recursively)
				foreach($file in Get-ChildItem $item -Recurse -File -Force | % FullName) {
					# Calculate the relative file path
					$relative = (Resolve-Path $file -Relative).TrimStart(".\")
					# Add the file to the zip
					$null = [System.IO.Compression.ZipFileExtensions]::CreateEntryFromFile($Archive, $file, $relative, $Compression)
				}
				Pop-Location
			}
		}
	}
	end {
		$Archive.Dispose()
		Get-Item $ZipFilePath
	}
}


function Expand-ZipFile {
	#.Synopsis
	#  Expand a zip file, ensuring it's contents go to a single folder ...
	[CmdletBinding()]
	param(
		# The path of the zip file that needs to be extracted
		[Parameter(ValueFromPipelineByPropertyName=$true, Position=0, Mandatory=$true)]
		[Alias("PSPath")]
		$FilePath,

		# The path where we want the output folder to end up
		[Parameter(Position=1)]
		$OutputPath = $Pwd,

		# Make sure the resulting folder is always named the same as the archive
		[Switch]$Force
	)
	process {
		$ZipFile = Get-Item $FilePath
		$Archive = [System.IO.Compression.ZipFile]::Open( $ZipFile, "Read" )

		# Figure out where we'd prefer to end up
		if(Test-Path $OutputPath) {
			# If they pass a path that exists, we want to create a new folder
			$Destination = Join-Path $OutputPath $ZipFile.BaseName
		} else {
			# Otherwise, since they passed a folder, they must want us to use it
			$Destination = $OutputPath
		}

		# The root folder of the first entry ...
		$ArchiveRoot = ($Archive.Entries[0].FullName -Split "/|\\")[0]

		Write-Verbose "Desired Destination: $Destination"
		Write-Verbose "Archive Root: $ArchiveRoot"

		# If any of the files are not in the same root folder ...
		if($Archive.Entries.FullName | Where-Object { @($_ -Split "/|\\")[0] -ne $ArchiveRoot }) {
			# extract it into a new folder:
			New-Item $Destination -Type Directory -Force
			[System.IO.Compression.ZipFileExtensions]::ExtractToDirectory( $Archive, $Destination )
		} else {
			# otherwise, extract it to the OutputPath
			[System.IO.Compression.ZipFileExtensions]::ExtractToDirectory( $Archive, $OutputPath )

			# If there was only a single file in the archive, then we'll just output that file...
			if($Archive.Entries.Count -eq 1) {
				# Except, if they asked for an OutputPath with an extension on it, we'll rename the file to that ...
				if([System.IO.Path]::GetExtension($Destination)) {
					Move-Item (Join-Path $OutputPath $Archive.Entries[0].FullName) $Destination
				} else {
					Get-Item (Join-Path $OutputPath $Archive.Entries[0].FullName)
				}
			} elseif($Force) {
				# Otherwise let's make sure that we move it to where we expect it to go, in case the zip's been renamed
				if($ArchiveRoot -ne $ZipFile.BaseName) {
					Move-Item (join-path $OutputPath $ArchiveRoot) $Destination
					Get-Item $Destination
				}
			} else {
				Get-Item (Join-Path $OutputPath $ArchiveRoot)
			}
		}

		$Archive.Dispose()
	}
}

# Add the aliases ZIP and UNZIP
new-alias zip new-zipfile
# zip .\filename.zip folder-to-zip
new-alias unzip expand-zipfile

#Start-Transcript -Path $env:userprofile\PSTranscript.txt -Append

function appupdate {
  Write-Output "Checking for choco updates..."
  choco upgrade all -y
  scoop update
  scoop update starship
  winget upgrade Spotify
  winget upgrade Bitwarden
  winget upgrade GoLang.Go
  winget upgrade powershell
  winget upgrade GitHub.GitHubDesktop
  winget upgrade Git.Git
  winget upgrade Eugeny.Tabby
}

function newproj {
  #set-Location $PSScriptRoot
  $cwd = (Get-Location).Path
  set-Location $cwd
  Write-host "Setting up new project in: $cwd"
  
  pause

  $projname = Read-Host "What is your new projects name?"

  # .NET class creates folder if not exists for us. no need to test-path
  [System.IO.Directory]::CreateDirectory("$projname\$projname")

  Write-host "Changing to new folder $projname"
  set-Location $cwd\$projname
  Write-host "Creating blank README.md file"
  touch README.md
  "# $projname" | Add-Content README.md
  Write-host "Creating generic .gitignore file for go,node,batch,python,powershell,git"
  Invoke-WebRequest -UseBasicParsing "https://www.toptal.com/developers/gitignore/api/go,node,batch,python,powershell,git" -OutFile .gitignore
  Write-host "Initializing git"
  git init
}

Invoke-Expression (&starship init powershell)
clear-host
#Show-Notification "Loaded" "Profile Loaded at $(Get-Date -Format t)"
